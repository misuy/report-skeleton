#!/bin/bash

rm -rf tests
mkdir tests


echo "-------------TEST 1 (WRITE 1 100 times)-------------"
touch tests/test1_in
touch tests/test1_real_out
sum=0
for ((i = 0; i < 100; i++))
do
    x=$(printf '%x' 1)
    xx=$(printf '%x' $sum)
    echo -n -e \\x$x >> tests/test1_in
    echo -n -e \\x$xx >> tests/test1_real_out
    sum=$((($sum + 1) % 256))
done

dd if=/dev/zero of=/dev/vramdisk1 bs=100 count=1
dd if=tests/test1_in of=/dev/vramdisk1 bs=100 count=1
dd if=/dev/vramdisk1 of=tests/test1_out bs=100 count=1
diff -s tests/test1_out tests/test1_real_out


echo "-------------TEST 2 (WRITE x00 x01 .. xff)-------------"
touch tests/test2_in
touch tests/test2_real_out
sum=0
for ((i = 0; i < 256; i++))
do
    x=$(printf '%x' $i)
    xx=$(printf '%x' $sum)
    echo -n -e \\x$x >> tests/test2_in
    echo -n -e \\x$xx >> tests/test2_real_out
    sum=$((($sum + $i) % 256))
done

dd if=/dev/zero of=/dev/vramdisk5 bs=256 count=1
dd if=tests/test2_in of=/dev/vramdisk5 bs=256 count=1
dd if=/dev/vramdisk5 of=tests/test2_out bs=256 count=1
diff -s tests/test2_out tests/test2_real_out


echo "-------------TEST 3 (COPY 8MiB BETWEEN VIRT PARTITIONS)-------------"
dd if=/dev/vramdisk6 of=/dev/vramdisk2 bs=1M count=8 oflag=dsync

echo "-------------TEST 4 (COPY 8MiB BETWEEN PHYS AND VIRT PARTITIONS)-------------"
touch tests/test4_in
dd if=/dev/urandom of=tests/test4_in bs=1M count=8 > /dev/null
dd if=tests/test4_in of=/dev/vramdisk2 bs=1M count=8 oflag=dsync
