# Лабораторная работа 2

## Название
Разработка драйверов блочных устройств

## Цель работы
Разработать и реализовать драйвер блочного устройства для операционной системы Linux, который позволяет создать виртуальный жесткий диск в оперативной памяти и исследовать характеристики скорости передачи данных.

## Описание функциональности драйвера
Драйвер создает диск с двумя физическими (12 Мбайт, 18 Мбайт) и одним расширенным (20 Мбайт) разделами. Расширенный раздел разделен на два логических (4 Мбайт, 16 Мбайт) раздела. При записи, каждый последующий записываемый байт должен быть суммой предыдущих.
## Инструкция по сборке
- make build -- собрать модуль
- make insmod -- загрузить модуль в ядро
- make rmmod -- выгрузить модуль из ядра
- make clean -- очистить файлы, генерерируемые во время сборки и тестирования

## Инструкция пользователя
```shell
echo <data> > /dev/vramdisk<N>
cat /dev/vramdisk<N>

hexdump /dev/vramdisk<N>

dd if=<input_file> of=/dev/vramdisk<N>
dd if=/dev/vramdisk<N> of=<output_file>

make test
```

## Примеры использования
```shell
make insmod
#Device         Boot Start    End Sectors Size Id Type
#/dev/vramdisk1          1  24575   24575  12M 83 Linux
#/dev/vramdisk2      24576  61439   36864  18M 83 Linux
#/dev/vramdisk3      61440 102399   40960  20M  5 Extended
#/dev/vramdisk5      61441  69631    8191   4M 83 Linux
#/dev/vramdisk6      69633 102399   32767  16M 83 Linux

echo 12345 > /dev/vramdisk1
hexdump /dev/vramdisk1
#0000000 3100 9663 ffca 0000 0000 0000 0000 0000
#0000010 0000 0000 0000 0000 0000 0000 0000 0000
#*
#0bffe00

make test
#bash tests.sh
#-------------TEST 1 (WRITE 1 100 times)-------------
#1+0 records in
#1+0 records out
#100 bytes copied, 0.000223012 s, 448 kB/s
#1+0 records in
#1+0 records out
#100 bytes copied, 0.000227257 s, 440 kB/s
#1+0 records in
#1+0 records out
#100 bytes copied, 0.000237355 s, 421 kB/s
#Files tests/test1_out and tests/test1_real_out are identical
#-------------TEST 2 (WRITE x00 x01 .. xff)-------------
#1+0 records in
#1+0 records out
#256 bytes copied, 0.000226846 s, 1.1 MB/s
#1+0 records in
#1+0 records out
#256 bytes copied, 0.000179305 s, 1.4 MB/s
#1+0 records in
#1+0 records out
#256 bytes copied, 0.000185981 s, 1.4 MB/s
#Files tests/test2_out and tests/test2_real_out are identical
#-------------TEST 3 (COPY 8MiB BETWEEN VIRT PARTITIONS)-------------
#8+0 records in
#8+0 records out
#8388608 bytes (8.4 MB, 8.0 MiB) copied, 0.0315399 s, 266 MB/s
#-------------TEST 4 (COPY 8MiB BETWEEN PHYS AND VIRT PARTITIONS)-------------
#8+0 records in
#8+0 records out
#8388608 bytes (8.4 MB, 8.0 MiB) copied, 0.039056 s, 215 MB/s
#8+0 records in
#8+0 records out
#8388608 bytes (8.4 MB, 8.0 MiB) copied, 0.0241202 s, 348 MB/s
```
