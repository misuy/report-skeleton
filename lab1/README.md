# Лабораторная работа 1

**Название:** "Разработка драйверов символьных устройств"

**Цель работы:** Познакомится с принципами разработки драйверов символьных устройств в Linux

## Описание функциональности драйвера

Драйвер предоставляет возможность применения побитовых логических операций к паре 32-битных целых чисел.

Поддерживаемые операции:
- побитовое И (AND)
- побитовое ИЛИ (OR)
- побитовое исключающее ИЛИ (XOR)

Драйвер может сохранить до 100 результатов, при переполнении буфера старые результаты будут последовательно перезаписываться новыми.

## Инструкция по сборке

`make`

```shell
make -C /usr/src/linux-headers-5.15.149-0-virt M=/root/io/lab1 modules
make[1]: Entering directory '/usr/src/linux-headers-5.15.149-0-virt'
  CC [M]  /root/io/lab1/ch_drv.o
/root/io/lab1/ch_drv.c: In function 'my_read':
/root/io/lab1/ch_drv.c:51:3: warning: ISO C90 forbids mixed declarations and code [-Wdeclaration-after-statement]
   51 |   char *out = kmalloc(sizeof(char) * max_buffer_size, GFP_KERNEL);
      |   ^~~~
In file included from ./include/linux/kernel.h:19,
                 from ./include/linux/list.h:9,
                 from ./include/linux/module.h:12,
                 from /root/io/lab1/ch_drv.c:1:
./include/linux/kern_levels.h:5:25: warning: format '%d' expects argument of type 'int', but argument 2 has type 'ssize_t' {aka 'long int'} [-Wformat=]
    5 | #define KERN_SOH        "\001"          /* ASCII Start Of Header */
      |                         ^~~~~~
./include/linux/printk.h:422:25: note: in definition of macro 'printk_index_wrap'
  422 |                 _p_func(_fmt, ##__VA_ARGS__);                           \
      |                         ^~~~
/root/io/lab1/ch_drv.c:54:5: note: in expansion of macro 'printk'
   54 |     printk(KERN_ERR "Could not copy to user; pos %d; out %d", pos, out);
      |     ^~~~~~
./include/linux/kern_levels.h:11:25: note: in expansion of macro 'KERN_SOH'
   11 | #define KERN_ERR        KERN_SOH "3"    /* error conditions */
      |                         ^~~~~~~~
/root/io/lab1/ch_drv.c:54:12: note: in expansion of macro 'KERN_ERR'
   54 |     printk(KERN_ERR "Could not copy to user; pos %d; out %d", pos, out);
      |            ^~~~~~~~
./include/linux/kern_levels.h:5:25: warning: format '%d' expects argument of type 'int', but argument 3 has type 'char *' [-Wformat=]
    5 | #define KERN_SOH        "\001"          /* ASCII Start Of Header */
      |                         ^~~~~~
./include/linux/printk.h:422:25: note: in definition of macro 'printk_index_wrap'
  422 |                 _p_func(_fmt, ##__VA_ARGS__);                           \
      |                         ^~~~
/root/io/lab1/ch_drv.c:54:5: note: in expansion of macro 'printk'
   54 |     printk(KERN_ERR "Could not copy to user; pos %d; out %d", pos, out);
      |     ^~~~~~
./include/linux/kern_levels.h:11:25: note: in expansion of macro 'KERN_SOH'
   11 | #define KERN_ERR        KERN_SOH "3"    /* error conditions */
      |                         ^~~~~~~~
/root/io/lab1/ch_drv.c:54:12: note: in expansion of macro 'KERN_ERR'
   54 |     printk(KERN_ERR "Could not copy to user; pos %d; out %d", pos, out);
      |            ^~~~~~~~
./include/linux/kern_levels.h:5:25: warning: format '%d' expects argument of type 'int', but argument 2 has type 'ssize_t' {aka 'long int'} [-Wformat=]
    5 | #define KERN_SOH        "\001"          /* ASCII Start Of Header */
      |                         ^~~~~~
./include/linux/printk.h:422:25: note: in definition of macro 'printk_index_wrap'
  422 |                 _p_func(_fmt, ##__VA_ARGS__);                           \
      |                         ^~~~
/root/io/lab1/ch_drv.c:56:3: note: in expansion of macro 'printk'
   56 |   printk(KERN_INFO "Driver: read()%d: %s\n", pos, out);
      |   ^~~~~~
./include/linux/kern_levels.h:14:25: note: in expansion of macro 'KERN_SOH'
   14 | #define KERN_INFO       KERN_SOH "6"    /* informational */
      |                         ^~~~~~~~
/root/io/lab1/ch_drv.c:56:10: note: in expansion of macro 'KERN_INFO'
   56 |   printk(KERN_INFO "Driver: read()%d: %s\n", pos, out);
      |          ^~~~~~~~~
/root/io/lab1/ch_drv.c: In function 'my_write':
/root/io/lab1/ch_drv.c:70:3: warning: ISO C90 forbids mixed declarations and code [-Wdeclaration-after-statement]
   70 |   enum {
      |   ^~~~
./include/linux/kern_levels.h:5:25: warning: format '%d' expects argument of type 'int', but argument 2 has type 'ssize_t' {aka 'long int'} [-Wformat=]
    5 | #define KERN_SOH        "\001"          /* ASCII Start Of Header */
      |                         ^~~~~~
./include/linux/printk.h:422:25: note: in definition of macro 'printk_index_wrap'
  422 |                 _p_func(_fmt, ##__VA_ARGS__);                           \
      |                         ^~~~
/root/io/lab1/ch_drv.c:109:3: note: in expansion of macro 'printk'
  109 |   printk(KERN_INFO "%d\n", ind);
      |   ^~~~~~
./include/linux/kern_levels.h:14:25: note: in expansion of macro 'KERN_SOH'
   14 | #define KERN_INFO       KERN_SOH "6"    /* informational */
      |                         ^~~~~~~~
/root/io/lab1/ch_drv.c:109:10: note: in expansion of macro 'KERN_INFO'
  109 |   printk(KERN_INFO "%d\n", ind);
      |          ^~~~~~~~~
  MODPOST /root/io/lab1/Module.symvers
  CC [M]  /root/io/lab1/ch_drv.mod.o
  LD [M]  /root/io/lab1/ch_drv.ko
make[1]: Leaving directory '/usr/src/linux-headers-5.15.149-0-virt'
```

## Инструкция пользователя

```shell
make run
echo <string> > /dev/var7 # <string> should be your input
cat /dev/var7 # to read
make stop
```

## Примеры использования

```shell
localhost:~/io/lab1# echo "1&1" > /dev/var7 
localhost:~/io/lab1# echo "1|2" > /dev/var7 
localhost:~/io/lab1# echo "5^3" > /dev/var7 
localhost:~/io/lab1# cat /dev/var7 
6
3
1
0
...
0
```
