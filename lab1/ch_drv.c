#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>

#include <linux/module.h>
#include <linux/kernel.h>

#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/stat.h>
#include <linux/uaccess.h>
#include <linux/types.h>
#include <linux/inet.h>
#include <linux/net.h>

static dev_t first;
static struct cdev c_dev; 
static struct class *cl;
static int* buff = NULL;
static ssize_t buff_pos = 0;

#define BUFFER_SIZE 100

static int my_open(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: open()\n");
  return 0;
}

static int my_close(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: close()\n");
  return 0;
}

static ssize_t my_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  const int max_buffer_size = 12;
  ssize_t pos = (buff_pos + 2*BUFFER_SIZE - *off - 1) % BUFFER_SIZE;
  if (pos == buff_pos)
    return 0;
//  printk(KERN_INFO "Driver: read()%d: %d\n", *off, buff[*off]);
  char *out = kmalloc(sizeof(char) * max_buffer_size, GFP_KERNEL);
  snprintf(out, max_buffer_size, "%d\n", buff[pos]);
  if (copy_to_user(buf, out, max_buffer_size) != 0) {
    printk(KERN_ERR "Could not copy to user; pos %d; out %d", pos, out);
  }
  printk(KERN_INFO "Driver: read()%d: %s\n", pos, out);
  kfree(out);
  (*off) += 1;
  return max_buffer_size;
}

static ssize_t my_write(struct file *f, const char __user *buf,  size_t len, loff_t *off) {
  char* in = kmalloc(sizeof(char) * len, GFP_KERNEL);
  if (copy_from_user(in, buf, len)) {
    printk(KERN_ERR "couldn\'t copy");
    kfree(in);
    return -1;
  }

  enum {
    NULL_ = 0,
    XOR,
    AND,
    OR
  } type = NULL_;

  ssize_t ind = 0;
  int a, b;


printk(KERN_INFO "%s\n", in);

  for (ind = 0; ind < len; ++ind) {

//printk(KERN_INFO "%c\n", in[ind]);
    if (in[ind] == '&' || in[ind] == '^' || in[ind] == '|') {
      switch (in[ind]) {
        case '&':
          type = AND;
         break;
        case '^':
          type = XOR;
         break;
        case '|':
          type = OR;
         break;
      }
      break;
    }
  }

  if (type == NULL_) {
    printk(KERN_ERR "operation not found");
    kfree(in);
    return -EINVAL;
  }

  in[ind] = '\0';
  printk(KERN_INFO "%d\n", ind);


  if (kstrtoint(in, 10, &a) != 0 || kstrtoint(in + ind + 1, 10, &b) != 0) {
    printk(KERN_ERR "parse error");
    kfree(in);
    return -EINVAL;
  }

  switch (type) {
    case XOR:
      buff[buff_pos] = a ^ b;
      break;
    case AND:
      buff[buff_pos] = a & b;
      break;
    case OR:
      buff[buff_pos] = a | b;
      break;
    default:
      break;
  }

  buff_pos = (buff_pos + 1) % BUFFER_SIZE;
  printk(KERN_INFO "%d", buff[buff_pos]);

  (*off) += len;
  kfree(in);
  return len;
}

static struct file_operations mychdev_fops =
{
  .owner = THIS_MODULE,
  .open = my_open,
  .release = my_close,
  .read = my_read,
  .write = my_write
};
 
static int __init ch_drv_init(void)
{
    printk(KERN_INFO "Hello!\n");
    if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0)
	  {
		return -1;
	  }
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL)
	  {
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    if (device_create(cl, NULL, first, NULL, "var7") == NULL)
	  {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    cdev_init(&c_dev, &mychdev_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
	  {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }

    buff = kmalloc(sizeof(int) * BUFFER_SIZE, GFP_KERNEL);
    return 0;
}
 
static void __exit ch_drv_exit(void)
{
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    kfree(buff);
    printk(KERN_INFO "Bye!!!\n");
}
 
module_init(ch_drv_init);
module_exit(ch_drv_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Author");
MODULE_DESCRIPTION("The first kernel module");

