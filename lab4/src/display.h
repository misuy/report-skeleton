#ifndef _DISPLAY_H
#define _DISPLAY_H

#include "systemc.h"
#include "string.h"

#define DISPLAY_WIDTH 8
#define DISPLAY_HEIGHT 4
#define DISPLAY_SIZE (DISPLAY_WIDTH * DISPLAY_HEIGHT)

#define SPI_ADDR_SIZE 8
#define SPI_DATA_SIZE 8
#define SPI_TRANS_SIZE (SPI_ADDR_SIZE + SPI_DATA_SIZE)

#define DISP_CMD_REG_ADDR 0
#define DISP_SYMBOL_ADDR  1 
#define DISP_CURSOR_ADDR  2

// cmd
#define DISP_UPDATE_CMD 1
#define DISP_CLEAR_CMD  2
#define DISP_READ_CURSOR 3
#define DISP_DRAW_SQUARE_CMD 4
#define DISP_DRAW_TRIANGLE_CMD 5
#define DISP_DRAW_CIRCLE_CMD 6

SC_MODULE(Display)
{
    sc_in<bool>  sclk_i;
    sc_in<bool>  mosi_i;
    sc_out<bool> miso_o;
    sc_in<bool> cs_i;
    
    SC_HAS_PROCESS(Display);
    
    Display(sc_module_name nm);
    ~Display();

    void spi_receive();
    
private: 
    int cursor_reg;
    int data_rec_reg;
    int addr_reg;
    int data_reg;

    int rec_bits_ctr;

    char disp_buf[DISPLAY_SIZE+1]; // +1 = /0

    void update_cursor(int val);
    void update();
    void clear();

    inline void fill_buffer_square();
    inline void fill_buffer_triangle();
    inline void fill_buffer_circle();
};


#endif // _DISPLAY_H
